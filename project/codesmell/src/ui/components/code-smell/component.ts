import Component, { tracked } from '@glimmer/component';
import * as moment_ from 'moment';
let moment: any = (<any>moment_).default || moment_;

export default class CodeSmell extends Component {
  @tracked title = 'welcome to my hn app';
  @tracked stories = [];

  // vars for new item
  @tracked newitemTitle = '';
  @tracked newitemUrl = '';
  @tracked newitemText = '';

  // vars for holding view 
  @tracked isStoryView = false;
  @tracked isListView = false;
  @tracked isSubmitView = false;

  // vars for viewing story
  @tracked currentStoryId = null;
  @tracked story = { ago: '', comments: [] };
  @tracked storyText = '';  
  @tracked storyComments = [];  

  // var for posting comments
  @tracked commentText = ''

  // var login display
  @tracked loggedIn = false;  

  // vars for profile
  @tracked userProfile;

  webAuth = new auth0.WebAuth({
    domain: 'roryok.eu.auth0.com',
    clientID: 'aQpIapitM0j3V8r9-XIGLiORFazTb3CK',
    redirectUri: 'http://localhost:3335/',
    audience: 'https://roryok.eu.auth0.com/userinfo',
    responseType: 'token id_token',
    scope: 'openid profile'
  });


  constructor(options) {
    super(options);
    // handle return from authentication 
    this.handleAuthentication();
    this.isListView = true;
    this.isStoryView = this.isSubmitView = false;
    this.loadStories();
  }

  setSession(authResult) {
    console.log("setSession:", authResult)
    // Set the time that the access token will expire at
    var expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    );
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
    this.loggedIn = true;
    this.getProfile();
  }

  handleAuthentication() {
    var self = this;

    this.webAuth.checkSession({
      audience: 'https://roryok.eu.auth0.com/userinfo',
      scope: 'openid profile',
      responseType: 'token id_token'
    }, function (err, authResult) {
      // this never happens, either in chrome or firefox. Something wrong here.   
      console.log(err, authResult, "checkSession")
    });
    
    if (window.location.hash.length > 0) {
      // if we have a hash, parse it

      // check to see if we have a session 
      this.webAuth.parseHash({ hash: window.location.hash }, function(err, authResult) {
        if (err) 
        {
          return console.log(err);
        }

        if (authResult != null) {
          // The contents of authResult depend on which authentication parameters were used.
          // It can include the following:
          // authResult.accessToken - access token for the API specified by `audience`
          // authResult.expiresIn - string with the access token's expiration time in seconds
          // authResult.idToken - ID token JWT containing user profile information
          if (authResult && authResult.accessToken && authResult.idToken) {
            window.location.hash = '';
            this.loggedIn = true;
            this.getProfile();
            console.log("Logged in");
          } else if (err) {
            this.loggedIn = false;
            console.log(err);
            alert(
              'Error: ' + err.error + '. Check the console for further details.'
            );
          }
        
          self.webAuth.client.userInfo(authResult.accessToken, function(err, user) {
            // Now you have the user's information
          });
        }
      });    
    }
  }

  getProfile() {
    console.log('getProfile()');
    if (typeof this.userProfile === 'undefined' || ('nickname' in this.userProfile) == false ) {
      var accessToken = localStorage.getItem('access_token');
      if (!accessToken) {
        console.log('Access token must exist to fetch profile');
      }
      this.webAuth.client.userInfo(accessToken, (err, profile) => {
        console.log("gotProfile", profile)
        if (profile) {
          this.userProfile = profile;
          localStorage.setItem('user_profile', profile);
        }
      });
    }
  }

  logoutClick() {
    console.log("Logging Out", this.loggedIn)
    this.webAuth.logout({
      returnTo: 'http://localhost:3335/',
      clientID: 'some client ID here'
    });
    this.loggedIn = false;
  }

  loginClick() {
    console.log("Logging In", this.loggedIn)
    this.webAuth.authorize({
      audience: 'https://roryok.eu.auth0.com/userinfo',
      scope: 'openid profile',
      responseType: 'token',
      redirectUri: 'http://localhost:3335/',
    });
  }

  titleOnKeyUp(event){
    this.newitemTitle = event.target.value;
  }
  urlOnKeyUp(event){
    this.newitemUrl = event.target.value;
  }
  textOnKeyUp(event){
    this.newitemText = event.target.value;
  }
  commentOnKeyUp(event){
    this.commentText = event.target.value;
  }

  showStories() {
    this.currentStoryId = null;
    this.isStoryView = this.isSubmitView = false;
    this.isListView = true;
  }

  didComment() {
    console.log("Commenting Complete!")
  }

  async showStory(id) {
    this.currentStoryId = id;
    this.isStoryView = true;
    this.isListView = this.isSubmitView = false;
    // get story from API 
    let response = await fetch(`./stories/${id}`)
    this.story = await response.json();
    this.story.ago = moment(this.story["posted"]).calendar()
    this.story.comments = this.story.comments.map(x => { 
      x.ago = moment(x.posted).calendar(); 
      return x; 
    })
    console.log("showing " + id)
  }

  showForm() {
    this.currentStoryId = null;
    this.isStoryView = this.isListView = false;
    this.isSubmitView = true;
  }

  submitStory(e) {
    e.preventDefault();
    if (this.newitemTitle == '') {
      alert("please enter a title")
    } else if (this.newitemUrl == '' || this.newitemText == '') {
      alert("please enter a url or some text")
    } else {
      fetch('./stories', {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ title: this.newitemTitle, url: this.newitemUrl, text: this.newitemText, user: this.userProfile.nickname })
      })
      .then(res => { res.json()})
      .then(res => { 
        console.log(res); 
        this.loadStories() 
        // blank the form
        this.newitemTitle = '';
        this.newitemUrl = '';
        this.newitemText = '';
        this.showStory(res.storyId);
      })
    }
  }

  submitComment(e) {
    e.preventDefault();
    if (this.commentText == '') {
      alert("please enter a comment")
    } else {
      fetch(`./stories/${this.currentStoryId}/comments`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ storyId: this.currentStoryId, text: this.commentText, user: this.userProfile.nickname })
      })
      .then(res => { console.log(res); res.json()})
      .then(res => { 
        this.showStory(this.currentStoryId);
        // blank the comment box
        this.commentText = '';
      })
    }
  }

  async loadStories() {
    let response = await fetch('./stories');
    let json = await response.json();    
    this.stories = json.map(x => { 
      if (typeof x.url !== 'undefined'){        
        x.domain = x.url.replace('http://','').replace('https://','').split('/')[0];
      }      
      x.ago = moment(x.posted).calendar(); 
      return x; 
    });
  }

};
