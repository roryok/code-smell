import Component, { tracked } from '@glimmer/component';

export default class CommentForm extends Component {

  // var for posting comments
  @tracked commentText = ''

  constructor(options) {
    super(options);
    console.log("comment-form", options)
  }

  commentOnKeyUp(event){
    this.commentText = event.target.value;
  }

  submitComment(e) {
    e.preventDefault();
    if (this.commentText == '') {
      alert("please enter a comment")
    } else {
      fetch(`./stories/${this.args.story.id}/comments`, {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ storyId: this.args.story.id, text: this.commentText })
      })
      .then(res => { console.log(res); res.json()})
      .then(res => { 
        // blank the comment box
        this.commentText = '';
      })
    }
  }

};
