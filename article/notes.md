Express version 4 and above requires extra middle-ware layer to handle POST request. This middle-ware is called as ‘bodyParser’. This used to be internal part of Express framework but now you have to install it separately.


if you're running this on windows, you may need to fix a few things first 
npm install -g ember-cli-windows

build our backend app in a folder called api/main.js

note: naming is important here. I first created this as server.js in the root which caused me lots of trouble. If you create a folder called server or a file in the root called server.js, it will confuse ember-cli. Stick to the api naming convention for now. 

Windows doesn't play so well with ember. I had a few problems running "serve" and at times had to resort to the new bash function. It's a great feature! 

GlimmerJS is the view layer of Ember. 

The documentation on GlimmerJS.com is a little sparse in some areas. Luckily there's lots more on https://guides.emberjs.com that we can use. The "Templates" section in particular is very useful to us. Here we can learn how to do conditionals 

{{#if condition}}
{{/if}}

Ember conditionals are limited to true and false, and can't include comparisons like !=<> etc. In our case, we need to set up a boolean for each viewmode. 