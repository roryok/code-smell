# TL;DR;

*TL;DR;* GlimmerJS is a fast and lightweight UI component framework built by the Ember.js team. In this tutorial, we'll introduce the GlimmerJS JavaScript library, go on to build a simple example app, and finally add authentication it with Auth0. The final code can be found at [repo link].

## An introduction to GlimmerJS, and Ember

GlimmerJS is the rendering engine of the Ember.js framework. Ember.js is built for efficiency and speed, and is used all over the web and even in a few desktop applications such as Apple Music. GlimmerJS is a new, separate project which delivers only the rendering engine, allowing you to build client-side JavaScript apps, or use a different backend technology such as express, while still using Ember style components. 

## Our HackerNews clone: CodeSmell

We're going to build a very quick, very simple HackerNews clone, called CodeSmell. CodeSmell will show a list of interesting stories, sorted by date and user rating, and allow an authenticated user to post comments and new stories. 

![screen01](/Users/teamwork/Documents/auth0/article/screen01.png)

### Installing GlimmerJS, creating a base and our other prerequisites

GlimmerJS harnesses TypeScript and uses the ember-cli to compile your app and to create new components. The first thing we'll need to do is install ember-cli

```
$ npm install -g ember-cli
```

Next, we'll use ember-cli to create a glimmer blueprint - a base app to get us started 

```
$ ember new codesmell -b @glimmer/blueprint
```

Change to the codesmell directory

`$ cd codesmell`

We'll be using express as a backend, so be sure to install express. You'll also need to install `body-parser` in order for express to handle POST requests. We'll also install `shortid` to generate unique ids for us

`$ npm install --save express body-parser shortid`

We're also going to use Nodemon to serve files while testing. Install it globally.

`$ npm install -g nodemon`

> ### Important note about Windows
>
> Windows and ember-cli don't always play nice together. In particular, Windows Defender can cause ember-cli to run quite slowly as it scans every temp file created during the compilation process. Luckily there's a plugin for that. 
>
> Install ember-cli-windows globally and run it from your app directory. This will instruct Windows Defender to ignore your app folder, and speed up compilation time. 
>
> ```
> $ npm install -g ember-cli-windows
> $ ember-cli-windows
> ```
>

### Our App Directory Structure

Here's what our new app directory structure should look like 

    codesmell
    ├── config
    │   ├── environment.js
    │   ├── module-map.ts
    │   └── resolver-configuration.ts
    ├── dist/
    ├── node_modules
    ├── src
    │   ├── ui
    │   │   ├── components
    │   │   │   └── my-app
    │   │   │       ├── component.ts
    │   │   │       └── template.hbs
    │   │   ├── styles
    │   │   │   └── app.css
    │   │   └── index.html
    │   ├── index.ts
    │   └── main.ts
    ├── ember-cli-build.js
    │
    ... other files ...



## Index.html

Here's what our index.html template looks like

```Html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Hello Glimmer</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{content-for "head"}}

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="{{rootURL}}app.css">
    
    {{content-for "head-footer"}}
  </head>
  <body>
    {{content-for "body"}}

    <div class="container" id="app"></div>
    <script src="//cdn.auth0.com/js/auth0/8.9/auth0.min.js"></script>
    <script src="{{rootURL}}app.js"></script>
    <script src="//unpkg.com/jquery" type="text/javascript"></script>
    <script src="//unpkg.com/moment" type="text/javascript"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    {{content-for "body-footer"}}
  </body>
</html>

```

We're using handlebars for templating. We'll use bootstrap for styles, add in jquery and moment.js for date handling. You'll see we've also added auth0.min.js for authentication. We'll come back to this later =)

## Build our API 

We'll need a simple express API which will serve a list of stories and allow us to post new ones and add comments. Create a new top level folder in the root called 'api'. 


> Note: naming is important here. I first created this as server.js in the root which caused lots of trouble. Ember-cli watches for files called server.js or folders called server. Stick to this api naming convention for now. 



Within our `api` subfolder, create a file called `main.js`. This is where all our API methods will live. 

```js
// require express, body-parser and shortid
// we could use es6 import here, but if we stick to the old style require syntax here it serves two purposes
// 1. there's no need to transpile our main.js before serving it up
// 2. it helps us see at a glance that we're dealing with server side vs client side
const express = require('express')
const bodyParser = require('body-parser')
const shortid = require('shortid');

// instantiate express
const app = express()

// create an array of stories and populate it with a few examples
let stories = [
  { id: 'hwX6aOr7', user: 'joe', posted: '2017-10-05 22:00:00', title: 'ThinkPad Anniversary Edition 25: Limited Edition ThinkPad Goes Retro', text: 'As part of its celebration of the 25th anniversary of the ThinkPad lineup, Lenovo is launching a limited-edition ThinkPad Anniversary Edition 25. It feels like Lenovo might have an issue with this though, because ThinkPad loyalists are all going to want one of these exclusive devices. It features a retro look, along with some of the retro capabilities that ThinkPad has always been known for, but all built on a modern version of the device.' },
  { id: 'nYrnfYEv', user: 'donna', posted: '2017-10-05 19:00:00', title: 'Japanese Vending Machines at Night Juxtaposed with a Wintry Hokkaido Landscape', url: 'http://www.spoon-tamago.com/2017/10/04/japanese-vending-machines-at-night-juxtaposed-with-a-wintry-hokkaido-landscape/' }
];

// create an array of comments and add some sample data
// note: each comment has a storyId of one of the stories above!
let comments = [ 
  { id: 'a4vhAoFG', storyId: 'hwX6aOr7', posted: '2017-10-05 22:11:00', user: 'podge', text: 'Someone please bring back 14" Laptops with 4:3 screens!' },
  { id: '2WEKaVNO', storyId: 'nYrnfYEv', posted: '2017-10-05 19:19:00', user: 'rodge', text: 'Getting a hot chocolate out of a vending machine while the sky is dumping snow is one of life\'s many sublime pleasures.' }
]

// configure express to use body-parser as middle-ware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// allow static files in the 'dist' folder to be served
app.use(express.static(__dirname + '/../dist'));

// load our main app 
app.get('/', function (req, res) {
  res.sendfile('../dist/index.html')
})

// GET a list of stories
app.get('/stories', function (req, res) {
  // return the array of stories
  res.send(stories)  
})

// GET a single story by storyId
app.get('/stories/:storyId', function (req, res) {
  // in a real-world app you'd need to have some proper error handling in here, 
  // but this will do us for now
  let storyId = req.params.storyId;
  // filter story by given storyId
  let story = stories.filter(x => { return x.id == storyId })[0];
  // get comments for this story
  story.comments = comments.filter(x => { return x.storyId == storyId })
  // return story
  res.send(story)  
})

// POST a new comment to a given story
app.post('/stories/:storyId/comments', function (req, res) {
  // get vars from params / body
  let storyId = req.params.storyId;
  let text = req.body.text;
  let user = req.body.user;
  // create comment object
  let comment = { 
    id: shortid.generate(), 
    posted: new Date().toISOString(), 
    storyId, 
    text, 
    user 
  }    
  // push it to the comments array 
  comments.push(comment);
  // get story
  let story = stories.filter(x => { return x.id == storyId })[0];
  // get comments
  story.comments = comments.filter(x => { return x.storyId == storyId })
  // return relevant story
  res.send(story)  
})

// POST a new story
app.post('/stories', function (req, res) {
  // get vars from body
  let title = req.body.title;
  let url = req.body.url;
  let text = req.body.text;
  let user = req.body.user;
  // create story object 
  let story = { 
    id: shortid.generate(), 
    posted: new Date().toISOString(), 
    title, 
    text, 
    url, 
    user 
  }
  // push it to the stories array
  stories.push(story)
  // return created story
  res.send(story)  
})

let port = process.env.port || process.env.PORT || 3335
app.listen(port, function () {
  console.log(`codesmell is now running on port ${port}`)
})
```


# Building our components

Now that we've constructed our data API, the next thing we need to do is build our core components. Let's jump right into the thick of it with a component that lists our stories array.

Create a new component with the following ember-cli command 

`$ ember g glimmer-component story-list`

This will create an empty story-list component. An empty glimmer component consists of three files:


	story-list
	├── component-test.ts
	├── component.ts
	└── template.hbs

Don't worry about `component-test.ts` for now - that's for unit-testing. Our main focus is the `component.ts` file, and the `template.hbs`

`Component.ts` is a TypeScript file which contains all our logic for our new component, and looks like this

```js
import Component, { tracked }  from '@glimmer/component';

export default class StoryList extends Component {
  constructor(options) {
	super(options);
  }
};
```

> If you're not familiar with TypeScript, it's a superset of JavaScript developer by Microsoft which adds static typing and compiles down to JavaScript. The best thing about it is you don't have to know it, you can just write JavaScript as normal and it'll compile down nicely. You can read up more about TypeScript [here](http://www.typescriptlang.org/index.html)

Note that even though we called it `story-list`, our class name has no hyphen, and has been converted to title case: `StoryList`. 

`template.hbs` is our handlebars template file which contains our all our presentation / UI stuff for this component. 

Right now it's not very exciting 

```Html
<div></div>
```

Change it to this:

```html
<div>Hello Glimmer!</div>
```

Let's move back to our main view file and embed this component there so we can actually see it. In our root `template.hbs`, enter the following HTML

```html
<story-list></story-list>
```

Now, if you open up a terminal in your app folder and run `$ ember serve`, the project should compile. Open up a second terminal, navigate to the api folder and run `nodemon main.js`. You should then be able to open the app in your browser at `localhost:3335`

![screen02](/Users/teamwork/Documents/auth0/article/screen02.png)

Not very exciting is it? Let's load in our stories. 

go back to your view's component.ts and add the following code

```javascript
import Component, { tracked } from '@glimmer/component';

export default class CodeSmell extends Component {

@tracked stories = [];

  constructor(options) {
	super(options);
	this.loadStories();
  }

  async loadStories() {
	// do a fetch request to our API
	let response = await fetch('./stories');
	// return a list of stories
	this.stories = await response.json();    	
  }

}
```

You'll notice we imported `{ tracked }` from glimmer. `@tracked` is a decorator in Glimmer you can add to properties to track changes in them. If we add any stories to this story array, our virtual DOM will re-render any UI that uses `stories` automatically. 

Go back to our root `template.hbs`, change how we instantiate story-list 

```Html
<story-list @stories={{ stories }}></story-list>
```

What we're saying here is "pass in the stories array from our view as a tracked property". 

Now open the template for `story-list` and scrub that silly "Hello Glimmer"


```html
<ul class="stories">
  {{#each @stories key="@index" as |story|}}
	<li>
	  {{#if story.url}}
	  <a href={{ story.url }} target={{ story.id }}>{{story.title}}</a>
	  {{else}}
	  <a href='#' onclick={{ action @showStory story.id }}>Discussion: {{story.title}}</a>
	  {{/if}}
	  <div>
		<a href='#' onclick={{ action @showStory story.id }}>{{ story.posted }} by {{ story.user }} | comments</a>
	  </div>
	</li>
  {{/each}}
</ul>
```

This code loops over our provided `stories` array using Glimmer / Ember #each syntax. Each story is returned as an object, and we can use handlebars to output the properties of that object. So the output will look like this


```Html
<ul class="stories">
	<li>
	  <a href="#">Discussion: ThinkPad Anniversary Edition 25: Limited Edition ThinkPad Goes Retro</a>
	  <div>
		<a href="#">2017-10-05 22:00:00 by joe | comments</a>
	  </div>
	</li>
	<li>
	  <a href="http://www.spoon-tamago.com/2017/10/04/japanese-vending-machines-at-night-juxtaposed-with-a-wintry-hokkaido-landscape/" target="nYrnfYEv">Japanese Vending Machines at Night Juxtaposed with a Wintry Hokkaido Landscape</a>
	  <div>
		<a href="#">2017-10-05 19:00:00 PM by donna | comments</a>
	  </div>
	</li>
</ul>
```


We've got two types of story just like HN - text, or url. Text stories will show our story text when clicked, and allow comments (eventually). URL stories will go straight to the URL, but you'll be able to click on the comments link on the bottom (again, eventually)

We handle each case by using a glimmer `#if` declaration, which allows us to check against a boolean condition and render or not render part of the page 



> Glimmer is a little limited in its use of `#if`, in that it can only work on the value of a boolean. So for example, you can't write something like this in glimmer: 
>
> 	{{#if stories.length > 0 }}
> 	{{/if}}
>
> In order to do something like that, you'd have to first create a separate boolean in logic and assign it the vallue of `stories.length > 0` 
>
> 	@tracked hasStories = stories.length > 0;
>
> and then 
>
> 	{{#if hasStories }}
> 	{{/if}}
>
> This is a little inconvenient, but it's easy enough to work around. 



Of course, if you click on a link right now, nothing happens. This is because we forgot to pass in our action method, `@showStory`, from this line

```Html
<a href='#' onclick={{ action @showStory story.id }}>{{ story.posted }} by {{ story.user }} | comments</a>
```

We need to pass this in from our view. We *could* write a `showStory` method within our story-list component. That would then work some UI magic and replace the content of story-list with our story. But organisationally, that's a little messy. It makes more sense (in my opinion) to have a separate `story-view` component referenced from our main component, and pass `showStory` in from there. 

> Note: There appears to be an issue (on Windows at least) naming components as single words. For example, if you called your story view just 'story' or 'view' it could result in compilation errors. I find it best to play it safe and give every component a hyphenated name like story-list, story-view etc. 

In root/component.ts add a `showStory` method like this


```js
@tracked story = {};  
async showStory(id) {
	this.currentStoryId = id;
	// get the story from API 
	let response = await fetch(\`./stories/${id}\`)
	this.story = await response.json();      
	console.log("showing story:", this.story)	
}
```

Then in `template.hbs`:

```html
<story-list @stories={{ stories }} @showStory={{ action showStory }}></story-list>
```

Make sure to prefix `showStory` with the action helper to make sure the correct 'this' context is passed through to the component. Now when we refresh the page and click on a story we should see our story pop up in the console, complete with comments. We're getting somewhere! 

## Build a Story View Component 

Hopefully we're starting to get quite familiar with the Glimmer JS way of working. Create another new component. 

` $ ember g glimmer-component story-view`

Declare it in our root component template

```html
<story-view @story={{ story }}></story-view>
```

Of course, we don't want both the list and story-view showing at the same time, so lets put some of that 'if' logic in to show and hide these alternately

component.ts

```js
@tracked isShowingStoryView = false;
@tracked isShowingStoryList = true; // default to showing the list

async showStory(id) {
	this.currentStoryId = id;
	// get the story from API 
	let response = await fetch(`./stories/${id}`)
	this.story = await response.json();      
	console.log("showing story:", this.story)	
	// change the visible sections
	this.isShowingStoryView = true;
	this.isShowingStoryList = false; 
}  

async loadStories() {
	// do a fetch request to our API
	let response = await fetch('./stories');
	// return a list of stories
	this.stories = await response.json();  
	this.isShowingStoryView = false;
	this.isShowingStoryList = true;   	
}
```

template.hbs

```handlebars
{{ #if isShowingStoryView }}
<story-view @story={{ story }}></story-view>
{{ /if }}

{{ #if isShowingStoryView }}
<story-list @stories={{ stories }} @showStory={{ action showStory }}></story-list>
{{ /if }}
```


Now, when we 'show' a story, the list should automatically hide. Nice!

But we've got another problem. How do we get back to the list after we've clicked on a story? We need some navigation 

## Building a nav bar

We don't really need a separate component for this. We *could* create one, but then we'd need to pass in a lot of arguments. Let's keep it simple for this example. This is based on the nav sample from the bootstrap documentation:

```Html
<nav class="navbar navbar-toggleable-md navbar-light">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" 
    data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#" onclick={{ action showStories }}><span>Ω&nbsp;</span>Codesmell</a>
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#" onclick={{ action showStories }}>stories</a>
      </li>
      {{#if loggedIn}}
      <li class="nav-item">
        <a class="nav-link" href="#" onclick={{ action showForm }}>submit</a>
      </li>
      {{/if}}      
    </ul>
    <ul class="navbar-nav">
      {{#if loggedIn}}      
      <li class="nav-item">
        <span>{{ userProfile.nickname }}</span>
      </li>
      <li class="nav-item">              
        <a href='#' class="nav-link" onclick={{ action logoutClick }}>Log Out</a>
      </li>
      {{else}}
      <li class="nav-item">              
        <a href='#' class="nav-link" onclick={{ action loginClick }}>Log In</a>
      </li>
      {{/if}}      
    </ul>
  </nav>
```

This goes in `code-smell/template.hbs`  above your components


# Authenticating your app 

What about submitting new stories? Commenting? 

We can't let just *anyone* post a story - users need to be logged in first. We'll use [Auth0 Hosted Login](https://auth0.com/docs/hosted-pages/login) for this as it's one of the easiest ways to authenticate users on our app. We've already added a link to it in index.html, now let's add this code to `code-smell/component.ts`

```js

  webAuth = new auth0.WebAuth({
    domain: 'mydomain.eu.auth0.com',
    clientID: 'aQpIfhRUndV8r9-SOUkjsdkfh3CK',
    redirectUri: 'http://localhost:3335/',
    audience: 'https://mydomain.eu.auth0.com/userinfo',
    responseType: 'token id_token',
    scope: 'openid profile'
  });

  constructor(options) {
    super(options);
    this.handleAuthentication();
    this.isListView = true;
    this.isStoryView = this.isSubmitView = false;
    this.loadStories();
  }

  setSession(authResult) {
    // Set the time that the access token will expire at
    var expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    );
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
    this.loggedIn = true;
    this.getProfile();
  }

  handleAuthentication() {
    var self = this;
    this.webAuth.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';
        this.setSession(authResult);
        this.loggedIn = true;
        console.log("Logged in");
      } else if (err) {
        this.loggedIn = false;
        console.log(err);
        alert(
          'Error: ' + err.error + '. Check the console for further details.'
        );
      }
    });
  }

  getProfile() {
    if (typeof this.userProfile === 'undefined' || ('nickname' in this.userProfile) == false ) {
      var accessToken = localStorage.getItem('access_token');
      if (!accessToken) {
        console.log('Access token must exist to fetch profile');
      }
      this.webAuth.client.userInfo(accessToken, (err, profile) => {
        if (profile) {
          this.userProfile = profile;
          localStorage.setItem('user_profile', profile);
        }
      });
    }
  }

  logoutClick() {
    console.log("Logging Out", this.loggedIn)
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('user_profile');
    this.loggedIn = false;
  }

  loginClick() {
    console.log("Logging In", this.loggedIn)
    this.webAuth.authorize();
  }
```

Neat! This should handle our login. `handleAuthentication` is called when the app is constructed, which calls the Auth0 Hosted Login code and handles login. 

Note that it also returns the users profile, so we have a nickname to work with =)

# Adding new Stories

`$ ember g glimmer-component story-submit`

UI FOR STORY SUBMIT

LOGIC 



# Commenting on Stories

`$ ember g glimmer-component comment-submit`

UI FOR COMMENT SUBMIT

LOGIC

![screen01](/Users/teamwork/Documents/auth0/article/screen01.png)

# Conclusion 

Hooray! You built a GlimmerJS app! Pat yourself on the back. 

GlimmerJS is a great framework for quickly building components on either client or server side. 

Auth0 Lock is super easy to use with it. 







