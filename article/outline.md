# TL;DR;

*TL;DR;* in this article we'll go introduce glimmerJs, explain why its great, and then build a simple hackernews clone with it.

# An introduction to GlimmerJS, and Ember

What's good about it, why you might use it etc.

# Our HackerNews Clone: CoderHeadlines

A little explanation of our app - a quick hackernews copy.

[screenshot]

# Installing GlimmerJS, creating a base and our other prerequisites

```
$ npm install -g ember-cli
$ ember new my-app -b @glimmer/blueprint
```

Using ember as a backend 

"install mongo locally, if you haven't already."

# Our App Directory Structure

quick intro to the folder structure created by ember-cli. the routes created by default, how to add more etc. 

# Build a basic schema in Mongoose

`$ npm install mongoose`

some sample db code - just hashing out most likely schemas...

```
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test', { useMongoClient: true, promiseLibrary: global.Promise });

var user = mongoose.model('User', { 
	username: String,
	password: String, // hashed 
	created: Date
});

var story = mongoose.model('Story', { 
	userId: ObjectId,
	title: String,
	url: String,
	text: String,
	votes: Number, // keep our algorithm simple for now
	// just the number of votes divided by the age in days
	created: Date
});

var comment = mongoose.model('Comment', { 
	storyId: ObjectId,
	parentCommentId: ObjectId,
	level: Number, // this makes it easier to sort our nested comments later
	text: String,
	created: Date
});

// throw in some sample data 
// var s = new Story({ title: 'Apple iPhone XII', url: 'http://apple.com/iphone' });
// s.save(function (err) {
//   if (err) {
//     console.log(err);
//   } else {
//     console.log('meow');
//   }
// });

```

# Show a list of items

the default route - "/" - will render a list of all stories, sorted by rank

# The list-item component

an easy intro to components - just output the link and votes for now 

# The list component

iterate over a list of items and render a <list-item> for each

# Show an individual story

create an ember route - "/story?id=1234" - to view a particular story

create an empty view 

add a component which shows the story itself

# The story component

when viewing the story, we can either go to the url, or view the comments / text. 

# The comment component

show any comments on this story. a comment can have a parent comment. we should return a list of all comments for this story, then sort them by level using our predefined level attribute.

"We'll parse that list of comments one level at a time, until we've built a comment tree. "

Comment tree sounds a bit puny, might reword that. 

each comment should have a reply button so you can reply directly to that specific comment. 

# Adding stories and comments

"So far all we've done is view static content. let's add a way for the user to submit stories and post comments"

create a route - "/submit" - to submit new stories

create a submit view

create a POST route for "/submit" - which actually handles the post

# The add-story component and route

input area for title, link, and optional textarea if this is a text item

create a route which accepts a posted story and adds it to the db 

# The comment component and route

input area for comment. 

create a route - "/comment" - which accepts and storyId and shows a comment box

at this point, go back to our comment list and wire up the comment 'reply' button. when a reader replies to a specific comment, a commentId will also be passed to /comment

when comment recieves a commentId it will also load that comment into the view for reference, like HN does

create a POST route on "/comment" which adds the comment. 

# Authenticating your app 

"We can't let just anyone post a story - users need to have an account first, and be signed in. we'll use Auth0 Hosted Login for this as it's one of the easiest ways to authenticate users on our app."

Setting up hosted login. (I've read through this but haven't implemented it quite yet)

Enabling Hosted Logins

Setting up Lock

Securing the story submission and comment pages from login 

Some pretty screenshots of our completed app here, with a few witty technews items from the near future

# Conclusion 

GlimmerJS is a great framework for quickly building components on either client or server side. 

Auth0 Lock is easy to use with it. 







